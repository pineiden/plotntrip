#!/bin/bash

SEPARATOR='|'
COLUMNAS='X_POS|Y_POS|Z_POS|CT_GPS_WEEK|GPS_MS_OF_WEEK|SIG_EAST|SIG_NORT|SIG_UP'

COLS_ARRAY=($(echo $COLUMNAS|sed 's/|/\n/g'))

FILE=$file
FILE_PATH='data/'$FILE

FILE_HEAD=$(head -n 1 $FILE_PATH)

ID_COLS=($(awk -F$SEPARATOR -v col="$COLUMNAS" 'BEGIN{
split(col,nums,"|")
}(NR==1){for (h in nums){		
for(i=1; i<=NF; i++)
{
if($i==nums[h]){
print nums[h]":"i
}	
}
}}' $FILE_PATH))


get_id(){
a=$1[@]
list=("${!a}")
column=$2
for i in ${list[*]};
do	
	echo $i | grep $column | awk -F':' '{print $2}'
done	
}


#export -f gps2date
#typeset -F

#tfinal=$(gps2date $1 $2)	
#year=$(date --date @$tfinal +%Y)
#Get GPS weekend and day---< convert to seconds
ID_X_POS=$(get_id ID_COLS ${COLS_ARRAY[0]})
ID_Y_POS=$(get_id ID_COLS  ${COLS_ARRAY[1]})
ID_Z_POS=$(get_id ID_COLS ${COLS_ARRAY[2]})
ID_GPS_WEEK=$(get_id ID_COLS ${COLS_ARRAY[3]})
ID_GPS_MS=$(get_id ID_COLS ${COLS_ARRAY[4]})
ID_SIG_E=$(get_id ID_COLS ${COLS_ARRAY[5]})
ID_SIG_N=$(get_id ID_COLS  ${COLS_ARRAY[6]})
ID_SIG_Up=$(get_id ID_COLS ${COLS_ARRAY[7]})


#watch -n 1 "tail -n 1  $FILE_PATH | awk -F'$SEPARATOR' -v gps_week=$ID_GPS_WEEK -v gps_ms=$ID_GPS_MS -v x=$ID_X_POS  -v y=$ID_Y_POS -v z=$ID_Z_POS '{
#gps_s=\$gps_ms/1000;
#command=\"./gps2date.sh \"\$gps_week\" \"gps_s\"\";
#command | getline d;
#close(command);
#print d", "\$x", "\$y", "\$z}'"

#Rescate X0,Y0,Z0
#llamar fn de conversion coord_geodesicas

#obtener segunda línea y generar los valores X0,Y0,Z0

R0=$(sed -e '2 ! d' $FILE_PATH | awk -F$SEPARATOR -v x=$ID_X_POS  -v y=$ID_Y_POS -v z=$ID_Z_POS 'BEGIN{OFS=",";} { print $x,$y,$z}')

#python -c "from libs.coordinates import print_coord; print_coord([X0,Y0,Z0],[X,Y,Z],separator='|')"

#Buscar R0 --> segunda línea de conexion

tailf -n 1  $FILE_PATH | awk -F$SEPARATOR -v gps_week=$ID_GPS_WEEK \
-v gps_ms=$ID_GPS_MS -v x=$ID_X_POS  -v y=$ID_Y_POS -v z=$ID_Z_POS \
-v sig_e=$ID_SIG_E -v sig_n=$ID_SIG_N -v sig_up=$ID_SIG_Up -v R0=$R0 '{
gps_s=$gps_ms/1000;
command="./gps2date.sh "$gps_week" "gps_s" ";
command | getline date;
close(command);
d=date-4*60*60;
R=$x","$y","$z;
command_latlon="python -c \"from libs.coordinates import print_coord; print_coord(["R0"],["R"]) \" ";
command_latlon | getline latlon;
close(command_latlon);
split(latlon, DEG_POS,"|");
lon=DEG_POS[1];
lat=DEG_POS[2];
h=DEG_POS[3];
print d,lon,$sig_e,lat,$sig_n,h,$sig_up}' > estacion.now
#ej
#ID_X_POS=$(echo ${ID_COLS['X_POS']}|awk -F= '{print $2}')



