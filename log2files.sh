#!/bin/bash

#Examples:
# ./log2files.sh -l="files/log_161201" -s="UDEC UAIB"
# ./log2files.sh -l="../OUTPUT/log_161123" -s="UDEC"
# ./log2files.sh -l="files/log_161201" -s="UDEC UAIB" -t0="12:20" -tf="12:40"


for i in "$@"
do
case $i in
    -l=*|--logfile=*)
    logfile="${i#*=}"
    shift # past argument=value
    ;;
    -s=*|--stationlist=*)
    stationlist="${i#*=}"
    shift # past argument=value
    ;;
    -t0=*)
    t0="${i#*=}"
    ;;
    -tf=*)
    tf="${i#*=}"
    ;;
esac
done

path=$logfile
export estaciones=$stationlist
export timestamp=$(date +%s)
export t0
export tf
echo $t0
echo $tf
echo $timestamp
awk -v est="$estaciones" -v ts=$timestamp '{
    id=$4;
    split(est,estaciones," "); 
    for (st in estaciones){
        if( id == estaciones[st] ){
            print $0 >> "files/" id"_" ts ".bnc"
            }
            }
    }' $path 


python bncplot.py
