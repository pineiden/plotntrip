#!/usr/bin/python

import datetime
import numpy as np
import matplotlib.pyplot as plt
import os
from multiprocessing import Pool
import re
import csv

# Timestamp to data format

def ts2datetime(value):
    day_time=value.split('_')
    day=day_time[0]
    time=day_time[1]
    ymd=list(map(int,day.split('-')))
    hms=list(map(int,list(map(float,time.split(':')))))
    dt=datetime.datetime(ymd[0],ymd[1],ymd[2],hms[0],hms[1],hms[2])
    return dt
    

est=os.environ['estaciones'].split(' ')
timestamp=os.environ['timestamp']
e_t0=os.environ['t0']
e_tf=os.environ['tf']
t0=datetime.datetime.strptime(e_t0, '%H:%M').time()
tf=datetime.datetime.strptime(e_tf, '%H:%M').time()

print(est)
pwd=os.getcwd()

path=os.path.dirname(os.path.abspath(__file__))

files=[]
for i in est:
    files.append("%s/files/%s_%s.bnc"% (pwd, i, timestamp))

print(files)


lat=''
lon=''

#p=Pool(8)
pattern=re.compile(r':')
v=1
for filename in files:
    x=[]
    y1=[]
    y2=[]
    y3=[]
    print(filename)
    with open(filename, newline='') as csvfile:
        data=csv.reader(csvfile, delimiter=' ')
        for row in data:
            if pattern.findall(row[2]):
                #filtrar por ventana de tiempo:
                #convert date time to time
                dt=ts2datetime(row[2])
                time_now=dt.strftime('%H:%M')
                time=datetime.datetime.strptime(time_now, '%H:%M').time()
                print(time)
                if time>=t0 and time<=tf:
                    x.append(dt)
                    y1.append(row[15])
                    y2.append(row[17])
                    y3.append(row[19])
                
        
        #try:
        #    dates=p.map(ts2datetime, x)
        #except Exception as exec:
        #    print(exec)
        dates=x
        print(len(dates))
        print(len(y1))

        try:
            fig = plt.figure(v)
            fig.suptitle(filename)

            plt.subplot(311)
            plt.xlabel("Time")
            plt.ylabel('N[m]')
            plt.title("North")
            plt.plot(dates, y1, c='b', label='North')
            plt.legend()
            plt.grid()

            plt.subplot(312)
            plt.xlabel("Time")
            plt.ylabel('E[m]')
            plt.title("East")
            plt.plot(dates, y2, c='r', label='East')
            plt.legend()
            plt.grid()

            plt.subplot(313)
            plt.xlabel("Time")
            plt.ylabel('U[m]')
            plt.title("Up")
            plt.plot(dates, y3, c='g', label='Up')
            plt.legend()
            plt.grid()           
            
            v +=1
        except Exception as exec:
            print(exec)
            
plt.show()
