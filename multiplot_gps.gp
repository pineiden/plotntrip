name="files/data.est"

set terminal qt size 1500,800

set multiplot layout 2,1 title "Estacion GPS: E-N"

set style data fsteps
set xlabel 'Tiempo'
set timefmt '%s'
set xdata time
set format x  '%Y-%m-%dT%H:%M%S'

set ylabel '[m]'

set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"

set rmargin 4

set title 'Este'
set key outside


plot name u 0:2 lt rgbcolor "blue"

set style data fsteps
set xlabel 'Tiempo'
set timefmt '%s'
set xdata time
set format x  '%Y-%m-%dT%H:%M%S'

set ylabel '[m]'

set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"

set rmargin 4

set title 'Norte'
set key outside


plot name u 0:4 lt rgbcolor "red"



unset multiplot
