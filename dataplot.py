#!/usr/bin/python

import datetime
import numpy as np
import matplotlib.pyplot as plt
import os
from multiprocessing import Pool

path=os.path.dirname(os.path.abspath(__file__))
filename=path+"/files/data.est"

import csv

x=[]
y1=[]
y2=[]
lat=''
lon=''
with open(filename, newline='') as csvfile:
    data=csv.reader(csvfile, delimiter=' ')
    row=[]
    for row in data:
        x.append(row[0])
        y1.append(row[1])
        y2.append(row[3])

    lat=row[2]
    lon=row[4]

# Timestamp to data format

def ts2datetime(value):
    return datetime.datetime.fromtimestamp(int(value))

# process in parallel
p=Pool(8)
date=p.map(ts2datetime, x)
#print(date)

print(date)

fig = plt.figure(1)

plt.subplot(211)
plt.xlabel("Time")
plt.ylabel('N[m]')
plt.title("Latitud "+lat)
plt.plot(date,y1, c='b', label='Lat')
plt.legend()
plt.grid()

plt.subplot(212)
plt.xlabel("Time")
plt.ylabel('E[m]')
plt.title("Longitud "+lon)
plt.plot(date,y2, c='r', label='Lon')
plt.legend()
plt.grid()

plt.show()
