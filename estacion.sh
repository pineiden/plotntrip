export filename=files/UDEC19222.nmea 
export N=500
echo $filename
#tailf -n 1 $filename | awk -F',' '{tag=substr($1,2,length($1));if (tag=="GPGGA") {printf("%.3f %.8f %s %.8f %s\n",$2,$3,$4,$5,$6)}}' > estacion.now

export estacion_now="estacion.now"

function data_est {
cat $filename |awk -F',' '{
        if (NR==1){
        a=6378137;
        f=1/298.257223563;
        b=a*(1-f);
        e2=(a^2-b^2)/a^2;
        pi=atan2(0,-1);
        }
        tag=substr($1,2,length($1));
        tid=$2;
        split($3,lat,".");
        split($5,lon,".");
        hlat=substr(lat[1],1,2);
        hlon=substr(lon[1],1,2);
        dlat=substr(lat[1],1,2)"."lat[2]/60;
        dlon=substr(lon[1],1,2)"."lon[2]/60;
        nlat=(hlat+dlat)*pi/180;
        nlon=(hlon+dlon)*pi/180;
        if(tag=="GPGGA"){
            if(NR==2){
                t0=$2;
                lat_0=nlat;
                lon_0=nlon;
                delta_lat=0;
                delta_lon=0;
                };
            if(NR>2){
                N=a/sqrt((1-e2*sin(nlat)^2));
                M=a*(1-e2)/sqrt(1-e2*sin(nlat)^2);
                Rm=sqrt(N*M)
                delta_lat=(nlat-lat_0)*Rm;
                delta_lon=(nlon-lon_0)*Rm;
            }
        }
        else if (tag="GPRMC"){
            bday=$10  
        };
        if ( NR % 2 == 0 ){
          day=substr(bday,1,2);
          month=substr(bday,3,2);
          year=substr(bday,5,2);
          hour=substr(tid,1,2);
          min=substr(tid,3,2);
          secs=substr(tid,5,2);
          date=year"-"month"-"day"T"hour":"min":"secs;
          date_str="date -u -d "date" +%s";
          cmd=date_str|getline d;
          close(date_str);
          printf("%s %.8f %s %.8f %s\n",d,delta_lat,$4,delta_lon,$6);
          close(d)}
        }' > files/data.est
echo files/data.est
}


export data=$(data_est)

echo $data
